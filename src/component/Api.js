import React, { useState, useEffect} from 'react';
import '../Api.css'

const Api = () => {
    const [game,setGame] = useState([])

    useEffect( ()=>{

        fetch('http://localhost:3050/games')
        .then(res =>{ return res.json()})
        .then(res =>{setGame(res.games);})
        .catch(function (error) {
            console.log(error);
        })

    }, [])

    if (game !== undefined){
        return (
            <div> 
                <table>
                    <thead>
                        <tr>
                            <th> Nom </th>
                            <th> Developpeur </th>
                        </tr>
                    </thead>
                    <tbody>
                        {game.map(res =>{
                            return(
                                <tr>
                                    <td>{res.name}</td>
                                    <td>{res.dev}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>

            </div>
        );

    }else{
        return (
            <div>
                <p>Chargement des données</p>
            </div>
        );
    }

};

export default Api;
