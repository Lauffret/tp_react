import React, {useState } from 'react';
import '../ToDoList.css'

const ToDoList = () => {
    const [item, setItem ]= useState(" ")
    const [liste, setListe]= useState([])

    const afficherListe = () => {
        return liste.map( item => {
            return(
                <div>
                    <li>
                        {item}
                    </li>
                </div>
            )
        })
       };

    const ajoutItem = () =>{
        return(
            <div>
                <h1>Ajouter des items à votre liste</h1>
                <br/>
                <input className = "input" type = "text" onChange = {(e) => setItem(e.target.value)} /> 
                <button className = "input"  onClick = { () => setListe( liste => [...liste ,item] ) }>Ajouter</button>
            </div>
        )
    }

    return (
        <div>
            {ajoutItem()}
            <h1>MY TODO LIST</h1>
            {afficherListe()}
        </div>
    );
};

export default ToDoList;