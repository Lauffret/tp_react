import './App.css';
import { BrowserRouter,Route, Link} from 'react-router-dom';
import ToDoList from './component/ToDoList';
import Api from './component/Api';

const ToDoListPage = () =>{
  return(
    <div>
      <ToDoList/>
      <Link className = "link" to = "/api" > Page Api </Link>
    </div>
  )
}
const ApiPage = () =>{
  return(
    <div>
      <h1> Page Api</h1>
      <Api/>
      <Link className = "link"  to = "/"> Page Todo List </Link>
    </div>
  )
}

function App() {
  return (
    <div className = "div">
      <BrowserRouter>
        <Route exact path = "/" component = {ToDoListPage}/>      
        <Route path = "/api" component = {ApiPage}/>
      </BrowserRouter>
    </div>
  );
}

export default App;
